package de.uni_bremen.st.quide;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * @author Artur
 */
public class RestService {

	private static final String DEBUG_URL = "http://localhost:8081";

	private static RestService restService;

	private RestApi restApi;

	private RestService() {

		RequestInterceptor requestInterceptor =
						request -> request.addHeader("Accept", "application/json");

		RestAdapter restAdapter = new RestAdapter.Builder()
						.setEndpoint(DEBUG_URL)
						.setRequestInterceptor(requestInterceptor)
						.setLogLevel(RestAdapter.LogLevel.NONE)
						.build();

		restApi = restAdapter.create(RestApi.class);
	}

	/**
	 * @return the single view of the rest service.
	 */
	public static RestService getInstance() {
		if (restService == null) {
			restService = new RestService();
		}
		return restService;
	}

	/**
	 * @return the api to connect to the user management.
	 */
	public RestApi getRestApi() {
		return restApi;
	}

	/**
	 * Provides methods to connect to the different endpoints of the user management server.
	 * <p>
	 *
	 * @author Artur
	 */
	public interface RestApi {

		@GET("/tokens/request") String requestToken(@Query("username") String username);

		@GET("/tokens/request") Observable<String> requestTokenObservable(@Query("username") String username);

		@GET("/tokens/{key}") Boolean verifyToken(@Path("key") String key);
	}
}

