package de.uni_bremen.st.quide;

import org.junit.Ignore;
import org.junit.Test;
import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Performance tests for client requests via rest with and without rx java.
 *
 * 1. synchronous without rx	~ 3,5 seconds
 * 2. synchronous with rx			~ 1,5 seconds
 * 3. asynchronous with rx		~ 1,1 seconds
 *
 * @author Artur
 */
@Ignore
public class RestServiceIT {

	RestService restService = RestService.getInstance();

	@Test
	public void testListObs() {
	}

	@Test
	public void request1000Tokens() {
		IntStream.range(1, 1000).forEach(value -> {
			String token = restService.getRestApi()
							.requestToken("Arti");
			process(value, token);
		});
	}

	@Test
	public void request1000TokensWithRx() {
		IntStream.range(1, 1000).forEach(value -> {
			Observable<String> token = restService.getRestApi()
							.requestTokenObservable("Arti");
			token.subscribe(processString(value), processError(),
							processFinished());

		});
	}

	@Test
	public void request1000TokensWithRxOnNewThread() {
		IntStream.range(1, 1000).forEach(value -> {
			Observable<String> token = restService.getRestApi()
							.requestTokenObservable("Arti");
			token.observeOn(Schedulers.newThread()).subscribe(processString(value), processError(),
							processFinished());

		});
	}

	private Action0 processFinished() {
		return () -> System.out.println("Finished");
	}

	private Action1<Throwable> processError() {
		return throwable -> System.out.println("Something went wrong!");
	}

	private Action1<String> processString(int value) {
		return tokenKey -> process(value, tokenKey);
	}

	private void process(int value, String tokenKey) {
		assertIsAlphaNumeric(tokenKey);
		System.out.println("Round " + value);
	}

	@Test
	@Ignore
	public void requestTokenForValidUser() {
		String token = restService.getRestApi()
						.requestToken("Arti");

		Boolean actual = restService.getRestApi()
						.verifyToken(token);

		assertThat(actual, is(true));
		assertThat(token.length(), is(26));

		assertIsAlphaNumeric(token);
	}

	@Test
	@Ignore
	public void requestTokenForInvalidUser() {
		String token = restService.getRestApi()
						.requestToken("Peter");

		Boolean actual = restService.getRestApi()
						.verifyToken(token);

		assertThat(actual, is(false));
		assertThat(token.length() <= 26, is(true));
	}

	private void assertIsAlphaNumeric(String token) {
		for (char c : token.toCharArray()) {
			assertThat(Character.isLetterOrDigit(c), is(true));
		}
	}

}
