package de.uni_bremen.st.quide;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Artur
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class AbstractTest {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
}
