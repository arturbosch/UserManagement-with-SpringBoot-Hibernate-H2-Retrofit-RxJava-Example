package de.uni_bremen.st.quide.utils;

import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Artur
 */
public class DateUtilsTest {

	private static final LocalDate LOCAL_DATE = LocalDate.of(2015, Month.DECEMBER, 15);
	private static final Date DATE = new Date(115, 11, 15);

	@Test
	public void formatLocalDateToString() throws Exception {
		Date format = DateUtils.asDate(LOCAL_DATE);

		assertThat(format, is(DATE));
	}

	@Test
	public void convertStringToLocalDate() throws Exception {
		LocalDate date = DateUtils.asLocalDate(DATE);

		assertThat(date, is(LOCAL_DATE));
	}
}
