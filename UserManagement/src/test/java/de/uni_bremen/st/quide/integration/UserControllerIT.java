package de.uni_bremen.st.quide.integration;

import de.uni_bremen.st.quide.AbstractControllerTest;
import de.uni_bremen.st.quide.transfer.RegistrationForm;
import de.uni_bremen.st.quide.transfer.UserTO;
import de.uni_bremen.st.quide.utils.DateUtils;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Integration test for token service.
 *
 * @author Artur
 */
@Transactional
public class UserControllerIT extends AbstractControllerTest {

	private RestService restService = RestService.getInstance();

	private RegistrationForm form = new RegistrationForm("Peter", "Peter", "a@b.de");

	@Test
	public void getAllUsers() throws IOException {
		List<UserTO> users = restService.getRestApi().getAllUsers().execute().body();

		assertThat(users.get(0).getId(), is(1L));
		assertThat(users.get(1).getId(), is(2L));
	}

	@Test
	public void getOneUser() throws IOException {
		LocalDate date = LocalDate.of(2014, Month.FEBRUARY, 14);
		UserTO userTO = restService.getRestApi().getSpecificUserWithId(String.valueOf(1L)).execute().body();

		assertThat(userTO.getId(), is(1L));
		assertThat(userTO.getName(), is("Artur"));
		assertThat(userTO.getRegisterDate(), is(DateUtils.format(date)));
	}

	@Test
	public void addUser() throws IOException {
		UserTO userTO = restService.getRestApi().addUser(form).execute().body();

		assertThat(userTO.getName(), is("Peter"));
		assertThat(userTO.getRegisterDate(), is(DateUtils.format(LocalDate.now())));
	}
}
