package de.uni_bremen.st.quide.integration;

import de.uni_bremen.st.quide.AbstractControllerTest;
import de.uni_bremen.st.quide.transfer.HiddenMessage;
import de.uni_bremen.st.quide.transfer.LoginForm;
import de.uni_bremen.st.quide.transfer.TokenBearer;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

import static junit.framework.TestCase.assertTrue;

/**
 * Integration test for token service.
 *
 * @author Artur
 */
@Transactional
public class TokenControllerIT extends AbstractControllerTest {

	private RestService restService = RestService.getInstance();

	@Test
	public void verifyToken() throws IOException {
		LoginForm form = new LoginForm("Artur", "Artur");
		TokenBearer token = restService.getRestApi().requestToken(form).execute().body();
		boolean valid = restService.getRestApi().verifyToken(token).execute().body();
		assertTrue(valid);
	}

	@Test
	public void secretApi() throws IOException {
		LoginForm form = new LoginForm("Artur", "Artur");
		TokenBearer token = restService.getRestApi().requestToken(form).execute().body();
		HiddenMessage body = restService.getRestApi()
				.hello(token.asAuthorizationHeader())
				.execute().body();

		System.out.println(body.msg);
	}
}
