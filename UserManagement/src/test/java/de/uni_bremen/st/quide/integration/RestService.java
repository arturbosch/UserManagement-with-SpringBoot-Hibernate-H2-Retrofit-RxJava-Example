package de.uni_bremen.st.quide.integration;

import de.uni_bremen.st.quide.controllers.ApiController;
import de.uni_bremen.st.quide.transfer.HiddenMessage;
import de.uni_bremen.st.quide.transfer.LoginForm;
import de.uni_bremen.st.quide.transfer.RegistrationForm;
import de.uni_bremen.st.quide.transfer.TokenBearer;
import de.uni_bremen.st.quide.transfer.UserTO;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

import java.util.List;

/**
 * @author Artur
 */
public class RestService {

	private static final String DEBUG_URL = "http://localhost:8081";

	private static RestService restService;

	private RestApi restApi;

	private RestService() {

		Retrofit restAdapter = new Retrofit.Builder()
				.baseUrl(DEBUG_URL)
				.addConverterFactory(JacksonConverterFactory.create())
				.build();

		restApi = restAdapter.create(RestApi.class);
	}

	/**
	 * @return the single view of the rest service.
	 */
	public static RestService getInstance() {
		if (restService == null) {
			restService = new RestService();
		}
		return restService;
	}

	/**
	 * @return the api to connect to the user management.
	 */
	public RestApi getRestApi() {
		return restApi;
	}

	/**
	 * @author Artur
	 */
	public interface RestApi {

		@GET("/users")
		Call<List<UserTO>> getAllUsers();

		@GET("/users/{id}")
		Call<UserTO> getSpecificUserWithId(@Path("id") String id);

		@POST("/users")
		Call<UserTO> addUser(@Body RegistrationForm form);

		@POST("/tokens/request")
		Call<TokenBearer> requestToken(@Body LoginForm form);

		@GET("/api/hello")
		Call<HiddenMessage> hello(@Header("Authorization") String header);

		@POST("/tokens/verify")
		Call<Boolean> verifyToken(@Body TokenBearer bearer);
	}
}

