package de.uni_bremen.st.quide.service;

import de.uni_bremen.st.quide.AbstractTest;
import de.uni_bremen.st.quide.models.User;
import de.uni_bremen.st.quide.services.UserService;
import de.uni_bremen.st.quide.transfer.RegistrationForm;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;

/**
 * @author Artur
 */
@Transactional
public class UserServiceTest extends AbstractTest {

	private LocalDate date = LocalDate.of(2014, Month.FEBRUARY, 14);

	@Autowired
	private UserService service;

	@Before
	public void setUp() {

	}

	@After
	public void tearDown() {
		// can be set
	}

	@Test
	public void findById() {
		User user = service.getUserById(1L);

		assertUserIsArtur(user);
	}

	private void assertUserIsArtur(User user) {
		assertThat(user.getId(), is(1L));
		assertThat(user.getName(), is("Artur"));
		assertThat(user.getRegisterDate(), is(date));
	}

	@Test
	public void findByUsername() {
		User user = service.getUserByName("Artur");

		assertUserIsArtur(user);
	}

	@Test
	public void findAll() {
		List<User> userList = service.getUserList();

		assertThat(userList, hasSize(2));
	}

	@Test
	public void addUser() {
		User user = service.addUser(new RegistrationForm("Peter", "Peter", "Peter@pan.de"));

		assertThat(user.getId(), is(3L));
		assertThat(user.getName(), is("Peter"));
		assertThat(user.getRegisterDate(), is(LocalDate.now()));
	}
}
