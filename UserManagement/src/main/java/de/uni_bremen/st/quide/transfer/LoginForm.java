package de.uni_bremen.st.quide.transfer;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Artur Bosch
 */
public class LoginForm {

	@NotNull
	@Size(max = 20)
	private String name;
	@NotNull
	@Size(max = 20)
	private String password;

	public LoginForm() {

	}

	public LoginForm(String name, String password) {
		this.name = name;
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}
}
