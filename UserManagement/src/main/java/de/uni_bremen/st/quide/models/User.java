package de.uni_bremen.st.quide.models;

import de.uni_bremen.st.quide.transfer.RegistrationForm;
import de.uni_bremen.st.quide.transfer.UserTO;
import de.uni_bremen.st.quide.utils.DateUtils;
import org.hibernate.validator.constraints.Email;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Artur
 */
@Entity
@Table(name = "Users")
public class User {

	public static final User INVALID;

	static {
		INVALID = new User();
		INVALID.id = -1L;
		INVALID.name = "INVALID";
		INVALID.password = "INVALID";
		INVALID.email = "INVALID@INVALID.de";
		INVALID.registerDate = DateUtils.asDate(LocalDate.now());
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "Username", nullable = false, length = 20, unique = true)
	private String name;

	@Column(name = "Pass", nullable = false, length = 20)
	private String password;

	@Column(name = "Email", nullable = false, unique = true)
	@Email
	private String email;

	@Column(name = "Register_Date", nullable = false)
	private Date registerDate;

	@ManyToMany
	@JoinTable(name = "User_Roles", joinColumns = {@JoinColumn(name = "user_id")},
			inverseJoinColumns = {@JoinColumn(name = "role_id")})
	private Set<Role> roles = new HashSet<>();

	public User() {
	} //JPA

	public User(RegistrationForm form, LocalDate registerDate) {
		this.name = form.getName();
		this.password = form.getPassword();
		this.email = form.getEmail();
		this.registerDate = DateUtils.asDate(registerDate);
		this.roles = Collections.singleton(new Role("User"));
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public LocalDate getRegisterDate() {
		return DateUtils.asLocalDate(registerDate);
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public boolean isValid() {
		return !this.equals(INVALID);
	}

	public UserTO toTransferable() {
		boolean notNull = Objects.nonNull(id);
		notNull |= Objects.nonNull(name);
		notNull |= Objects.nonNull(registerDate);
		return notNull ? new UserTO(this)
				: new UserTO(INVALID);
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", name='" + name + '\'' +
				", password='" + password + '\'' +
				", email='" + email + '\'' +
				", registerDate=" + registerDate +
				", roles=" + roles +
				'}';
	}

	@SuppressWarnings("SimplifiableIfStatement")
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		User user = (User) o;

		if (id != null ? !id.equals(user.id) : user.id != null) return false;
		if (name != null ? !name.equals(user.name) : user.name != null) return false;
		if (password != null ? !password.equals(user.password) : user.password != null) return false;
		if (email != null ? !email.equals(user.email) : user.email != null) return false;
		if (registerDate != null ? !registerDate.equals(user.registerDate) : user.registerDate != null) return false;
		return roles != null ? roles.equals(user.roles) : user.roles == null;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (registerDate != null ? registerDate.hashCode() : 0);
		result = 31 * result + (roles != null ? roles.hashCode() : 0);
		return result;
	}
}
