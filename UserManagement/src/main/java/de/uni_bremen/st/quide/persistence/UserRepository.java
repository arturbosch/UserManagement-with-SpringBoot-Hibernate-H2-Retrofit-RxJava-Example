package de.uni_bremen.st.quide.persistence;

import de.uni_bremen.st.quide.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Data handling interface for users.
 *
 * @author Artur
 */
@Component
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findById(Long id);

	Optional<User> findByName(@Param("Username") String username);

	Optional<User> findByNameAndPassword(@Param("Username") String username, @Param("Pass") String password);
}
