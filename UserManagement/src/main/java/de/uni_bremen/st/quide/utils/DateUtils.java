package de.uni_bremen.st.quide.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author Artur
 */
public class DateUtils {

	public static Date asDate(LocalDate localDate) {
		return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	public static LocalDate asLocalDate(Date date) {
		return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public static long format(LocalDate registerDate) {
		return registerDate != null ? asDate(registerDate).getTime() : -1;
	}

	public static LocalDate from(long millis) {
		return asLocalDate(new Date(millis));
	}

	/**
	 * Creates a date of current timestamp with the given amount of minutes as offset.
	 *
	 * @param expireInMin offset of the newly created date
	 * @return a date
	 */
	public static Date nowPlusMinutes(int expireInMin) {
		return new Date(System.currentTimeMillis() + (expireInMin * 60000));
	}
}
