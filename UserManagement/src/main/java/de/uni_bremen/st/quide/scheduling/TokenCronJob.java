package de.uni_bremen.st.quide.scheduling;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Deletes all stored tokens from the cache used in token service.
 *
 * @author Artur
 */
@Component
public class TokenCronJob {

	@Scheduled(cron = "0 0 * * * *")
	public void cron() {
		System.out.println("Token Cron Job!!!");
	}
}
