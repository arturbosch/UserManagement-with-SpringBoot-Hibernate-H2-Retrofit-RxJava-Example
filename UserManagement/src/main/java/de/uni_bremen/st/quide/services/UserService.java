package de.uni_bremen.st.quide.services;


import de.uni_bremen.st.quide.models.User;
import de.uni_bremen.st.quide.persistence.UserRepository;
import de.uni_bremen.st.quide.transfer.RegistrationForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * @author Artur
 */
@Service("userService")
public class UserService {

	private final UserRepository userRepository;

	@Autowired
	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public User getUserById(long id) {
		Optional<User> maybeUser = userRepository.findById(id);
		return maybeUser.orElse(User.INVALID);
	}

	public User getUserByName(String name) {
		Optional<User> maybeUser = userRepository.findByName(name);
		return maybeUser.orElse(User.INVALID);
	}

	public Optional<User> getUserByNameAndPassword(String name, String password) {
		return userRepository.findByNameAndPassword(name, password);
	}

	public List<User> getUserList() {
		return userRepository.findAll();
	}

	public User addUser(RegistrationForm form) {
		User newUser = new User(form, LocalDate.now());
		return userRepository.save(newUser);
	}

}
