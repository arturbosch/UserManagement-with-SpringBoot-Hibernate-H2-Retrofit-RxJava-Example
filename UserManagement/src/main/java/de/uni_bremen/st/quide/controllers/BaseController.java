package de.uni_bremen.st.quide.controllers;

import de.uni_bremen.st.quide.models.ErrorDetail;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * Used by all controllers to simplify integration tests.
 *
 * @author Artur
 */
public abstract class BaseController {

	protected NoSuchUserException throwNoSuchUser(String username) {
		return new NoSuchUserException("There is no user with name: " + username);
	}

	@ExceptionHandler(NoSuchUserException.class)
	public ErrorDetail error(HttpServletRequest request, Exception exception) {
		ErrorDetail error = new ErrorDetail();
		error.setStatus(HttpStatus.BAD_REQUEST.value());
		error.setMessage(exception.getLocalizedMessage());
		error.setUrl(request.getRequestURL().append("/error/101").toString());
		error.setErrorCode(101);
		return error;
	}

	@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "There is no user with this name!")
	protected class NoSuchUserException extends RuntimeException {
		NoSuchUserException(String message) {
			super(message);
		}
	}

}
