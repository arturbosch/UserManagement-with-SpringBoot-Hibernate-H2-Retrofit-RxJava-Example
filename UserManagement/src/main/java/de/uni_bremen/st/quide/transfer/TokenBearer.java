package de.uni_bremen.st.quide.transfer;

/**
 * @author Artur Bosch
 */
public class TokenBearer {

	public static final String BEARER_KEYWORD = "Bearer ";

	private String token;

	public TokenBearer() {
	}

	public TokenBearer(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public String asAuthorizationHeader() {
		return BEARER_KEYWORD + token;
	}
}
