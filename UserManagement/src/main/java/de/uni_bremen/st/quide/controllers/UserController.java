package de.uni_bremen.st.quide.controllers;

import de.uni_bremen.st.quide.models.User;
import de.uni_bremen.st.quide.services.UserService;
import de.uni_bremen.st.quide.transfer.RegistrationForm;
import de.uni_bremen.st.quide.transfer.UserTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Rest controller to request information about users.
 *
 * @author Artur
 */
@RestController
@RequestMapping("/users")
public class UserController extends BaseController {

	private final UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<UserTO> listUsers() {
		List<User> userList = userService.getUserList();
		return userList.stream()
				.map(User::toTransferable)
				.collect(Collectors.toList());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public UserTO getUserById(@PathVariable Long id) {
		return userService.getUserById(id).toTransferable();
	}

	@RequestMapping(method = RequestMethod.POST)
	public UserTO addUser(@RequestBody @Valid RegistrationForm form) {
		User user = userService.addUser(form);
		return user.toTransferable();
	}

}
