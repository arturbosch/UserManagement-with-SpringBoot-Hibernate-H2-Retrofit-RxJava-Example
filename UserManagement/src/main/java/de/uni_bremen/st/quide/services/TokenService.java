package de.uni_bremen.st.quide.services;


import de.uni_bremen.st.quide.models.User;
import de.uni_bremen.st.quide.transfer.LoginForm;
import de.uni_bremen.st.quide.utils.DateUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.impl.crypto.RsaProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.KeyPair;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

/**
 * Implementation of token service.
 *
 * @author Artur
 */
@Service("tokenService")
public class TokenService {

	private static final String USER_ROLE = "USER";

	private final UserService userService;
	private final KeyPair keyPair;
	private final String kid;

	@Autowired
	public TokenService(UserService userService) {
		this.userService = userService;
		keyPair = RsaProvider.generateKeyPair(1024);
		kid = UUID.randomUUID().toString();
	}

	/**
	 * Requests a jwt for given login data.
	 *
	 * @param form name and password of an user
	 * @return maybe a token if the user exists
	 */
	public Optional<String> requestToken(LoginForm form) {
		return userService.getUserByNameAndPassword(form.getName(), form.getPassword())
				.map(User::getName)
				.map(this::generateForUser);
	}

	/**
	 * Verifies if given token was signed by this token service.
	 *
	 * @param token a jwt
	 * @return true it is not expired and signed by this service
	 */
	public boolean verifyToken(String token) {
		return verify(token).isPresent();
	}

	/**
	 * Verifies if given token was signed by this token service.
	 *
	 * @param token a jwt
	 * @return maybe the claims of the token if valid
	 */
	public Optional<Claims> verify(String token) {
		try {
			return Optional.of(Jwts.parser().setSigningKey(keyPair.getPrivate()).parseClaimsJws(token).getBody());
		} catch (ExpiredJwtException | MalformedJwtException |
				SignatureException | IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	/**
	 * Generates a basic json web token for the given name. This token is valid for 60 minutes and
	 * allows to access endpoints which can be used by the role USER. (There are no more roles at the
	 * time.)
	 *
	 * @param name name of the user for which the token is created
	 * @return json web token
	 */
	public String generateForUser(String name) {
		return generate(USER_ROLE, name, DateUtils.nowPlusMinutes(60));
	}

	/**
	 * Generates a json web token for the given role and the given user. This token expires when the
	 * current time is greater as the provided date.
	 *
	 * @param forRole role
	 * @param forUser username
	 * @param expire  date when to expire the token
	 * @return json web token
	 */
	public String generate(String forRole, String forUser, Date expire) {
		return Jwts.builder()
				.signWith(SignatureAlgorithm.RS256, keyPair.getPrivate())
				.setHeaderParam("kid", kid)
				.setSubject(forUser)
				.setAudience(forRole)
				.setExpiration(expire)
				.compact();
	}

}
