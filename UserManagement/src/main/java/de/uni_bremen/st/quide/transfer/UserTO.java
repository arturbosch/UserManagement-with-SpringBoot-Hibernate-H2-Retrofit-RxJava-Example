package de.uni_bremen.st.quide.transfer;

import de.uni_bremen.st.quide.models.Role;
import de.uni_bremen.st.quide.models.User;
import de.uni_bremen.st.quide.utils.DateUtils;

import java.util.Set;

/**
 * @author Artur
 */
public class UserTO {

	private long id;
	private String name;
	private long registerDate;
	private Set<Role> roles;

	public UserTO(long id, String name, long registerDate, Set<Role> roles) {
		this.id = id;
		this.name = name;
		this.registerDate = registerDate;
		this.roles = roles;
	}

	public UserTO(User user) {
		this(user.getId(), user.getName(), DateUtils.format(user.getRegisterDate()), user.getRoles());
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public long getRegisterDate() {
		return registerDate;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	@Override
	public String toString() {
		return "UserTO{" +
				"id=" + id +
				", name='" + name + '\'' +
				", registerDate=" + registerDate +
				", roles=" + roles +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		UserTO userTO = (UserTO) o;

		return id == userTO.id;

	}

	@Override
	public int hashCode() {
		return (int) (id ^ (id >>> 32));
	}
}
