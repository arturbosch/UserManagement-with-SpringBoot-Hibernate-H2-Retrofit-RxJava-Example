package de.uni_bremen.st.quide.transfer;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Artur Bosch
 */
public class RegistrationForm {

	@NotNull
	@Size(max = 20)
	private String name;
	@NotNull
	@Size(max = 20)
	private String password;
	@Email
	private String email;

	public RegistrationForm() {
	}

	public RegistrationForm(String name, String password, String email) {
		this.name = name;
		this.password = password;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}
}
