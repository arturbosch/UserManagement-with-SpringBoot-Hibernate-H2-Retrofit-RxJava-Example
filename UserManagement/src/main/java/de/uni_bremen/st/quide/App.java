package de.uni_bremen.st.quide;

import de.uni_bremen.st.quide.config.JwtClaimsFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Starts the application as a micro service with spring boot.
 *
 * @author Artur
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
public class App {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(App.class, args);
	}

	@Bean
	public FilterRegistrationBean addJwtClaimsFilter(JwtClaimsFilter filter) {
		FilterRegistrationBean bean = new FilterRegistrationBean();
		bean.setFilter(filter);
		bean.addUrlPatterns("/api/*");
		return bean;
	}

}
