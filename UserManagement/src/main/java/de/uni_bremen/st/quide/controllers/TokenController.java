package de.uni_bremen.st.quide.controllers;

import de.uni_bremen.st.quide.services.TokenService;
import de.uni_bremen.st.quide.transfer.LoginForm;
import de.uni_bremen.st.quide.transfer.TokenBearer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Rest controller to request information about tokens.
 *
 * @author Artur
 */
@RestController
@RequestMapping("/tokens")
public class TokenController extends BaseController {

	private final TokenService tokenService;

	@Autowired
	public TokenController(TokenService tokenService) {
		this.tokenService = tokenService;
	}

	@PostMapping("request")
	public TokenBearer requestToken(@RequestBody @Valid LoginForm form) {
		return tokenService.requestToken(form)
				.map(TokenBearer::new)
				.orElseThrow(() -> throwNoSuchUser(form.getName()));
	}

	@PostMapping("verify")
	public boolean verify(@RequestBody TokenBearer bearer) {
		return tokenService.verifyToken(bearer.getToken());
	}
}
