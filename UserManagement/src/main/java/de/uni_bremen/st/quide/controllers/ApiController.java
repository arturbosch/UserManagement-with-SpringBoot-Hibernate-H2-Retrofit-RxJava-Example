package de.uni_bremen.st.quide.controllers;

import de.uni_bremen.st.quide.transfer.HiddenMessage;
import io.jsonwebtoken.Claims;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Artur Bosch
 */
@RestController
@RequestMapping("api")
public class ApiController extends BaseController {

	@GetMapping("hello")
	public HiddenMessage hello(HttpServletRequest request) {
		Claims claims = (Claims) request.getAttribute("claims");
		String subject = claims.getSubject();
		if (!subject.equals("Artur")) {
			throw throwNoSuchUser(subject);
		}
		return new HiddenMessage("Hello secured Api!");
	}

}
