package de.uni_bremen.st.quide.config;

import de.uni_bremen.st.quide.services.TokenService;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Artur Bosch
 */
@Component
public class JwtClaimsFilter extends GenericFilterBean {

	private final TokenService tokenService;

	@Autowired
	public JwtClaimsFilter(TokenService tokenService) {
		this.tokenService = tokenService;
	}

	@Override
	public void doFilter(ServletRequest request,
						 ServletResponse response,
						 FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		String auth = req.getHeader("Authorization");
		validateAuthHeader(auth);
		String token = auth.substring(7);

		Claims claims = tokenService.verify(token)
				.orElseThrow(() -> new ServletException("Invalid token!"));

		req.setAttribute("claims", claims);
		chain.doFilter(request, response);
	}

	private void validateAuthHeader(String auth) throws ServletException {
		if (!StringUtils.hasText(auth) || !auth.startsWith("Bearer ")) {
			throw new ServletException("Missing or invalid authorization header!");
		}
	}

}
