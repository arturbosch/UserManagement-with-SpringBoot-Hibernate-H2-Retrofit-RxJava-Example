package de.uni_bremen.st.quide.transfer;

/**
 * @author Artur Bosch
 */
public class HiddenMessage {
	public String msg;

	public HiddenMessage() {
	}

	public HiddenMessage(String msg) {
		this.msg = msg;
	}
}