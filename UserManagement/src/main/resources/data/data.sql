INSERT INTO Users (ID, Username, Pass, Email, Register_Date)
VALUES (1, 'Artur', 'Artur', 'artur@artur.de', '2014-02-14 00:00:00');
INSERT INTO Users (ID, Username, Pass, Email, Register_Date)
VALUES (2, 'Smarti', 'Artur', 'artur@artur.de', '2014-02-14 00:00:00');

INSERT INTO Role (ID, Name)
VALUES (1, 'User');
INSERT INTO Role (ID, Name)
VALUES (2, 'Admin');

INSERT INTO User_Roles (user_id, role_id)
VALUES (1, 2);
INSERT INTO User_Roles (user_id, role_id)
VALUES (2, 1);
